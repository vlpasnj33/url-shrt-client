import { Link } from "react-router-dom"

const Footer = () => {
  return (
    <footer className="bg-gray-900 p-2 mx-20 mt-8">
      <div className="flex items-center justify-center">
        <div className="text-white font-bold mr-4">2023 Your Website</div>
        <nav className="flex space-x-4">
          <Link to="/" className="text-gray-300 hover:text-white">Terms</Link>
          <Link to="/" className="text-gray-300 hover:text-white">Privacy Policy</Link>
        </nav>
      </div>
    </footer>
  )
}

export default Footer