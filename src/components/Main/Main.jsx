import { useState } from 'react';
import UrlForm from '../UrlForm';
import QrcodeForm from '../QrcodeForm';

const Main = () => {
  const [currentForm, setCurrentForm] = useState('urlShortener');

  const handleFormSwitch = (formType) => {
    setCurrentForm(formType);
  };

  return (
        <main className="flex items-center justify-center h-screen">
          <section className="bg-gray-100 p-6 rounded-lg shadow-md">
            <h2 className="text-3xl font-bold mb-6">Create Short Links, QR Codes</h2>

            {currentForm === 'urlShortener' && <UrlForm />}

            {currentForm === 'qrCode' && <QrcodeForm />}

            <div className="flex justify-between mt-4">
              <button
                className={`py-2 px-4 rounded ${
                  currentForm === 'urlShortener' ? 'bg-blue-500 text-white hover:bg-blue-600' : 'bg-gray-300 text-gray-700 hover:bg-gray-400'
                }`}
                onClick={() => handleFormSwitch('urlShortener')}
              >
                URL Shortener
              </button>
              <button
                className={`py-2 px-4 rounded ${
                  currentForm === 'qrCode' ? 'bg-green-500 text-white hover:bg-green-600' : 'bg-gray-300 text-gray-700 hover:bg-gray-400'
                }`}
                onClick={() => handleFormSwitch('qrCode')}
              >
                QR Code
              </button>
            </div>
          </section>
        </main>
  );
};

export default Main;
