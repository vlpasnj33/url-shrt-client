
const About = () => {
  return (
    <div>
      <h2>About Our Service</h2>
      
      <p>Welcome to our URL shortener and QR code generator service!</p>
  
      <h3>Our Story</h3>
      <p>We embarked on this journey to simplify the sharing of long URLs. Our founder, inspired by the need to share links without the hassles of lengthy strings, envisioned a solution.</p>
    
      <p>With a passion for efficiency, our service was created to provide users with concise and easily shareable links. Since our inception, we have been dedicated to making link management effortless for everyone.</p>
    
      <h3>URL Shortener</h3>
      <p>Our URL shortener is designed to make your links tiny and manageable. Whether you are a business, marketer, or individual, our service empowers you to create short, reliable URLs that make a big impact.</p>
    
      <h3>QR Code Generator</h3>
      <p>In addition to our URL shortener, we offer a QR code generator. Seamlessly convert your URLs into QR codes for quick and easy sharing. QR codes are a modern and efficient way to share information, and we are here to make that process hassle-free.</p>
    
      <h3>Why Choose Us?</h3>
      <ul>
        <li><strong>Efficiency:</strong> Our services are designed for simplicity, ensuring that managing links is a breeze for everyone.</li>
        <li><strong>Reliability:</strong> Trust in our reliable and secure platform for creating both short URLs and QR codes.</li>
        <li><strong>Innovation:</strong> Stay ahead with our innovative solutions that cater to your evolving link management needs.</li>
      </ul>
    
      <p>Thank you for choosing us for your URL shortening and QR code generation needs. We are here to help you make your links work for you!</p>
    </div>  
  )
}

export default About