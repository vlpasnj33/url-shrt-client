import { Link } from "react-router-dom"

const Header = () => {
  return (
    <header className="bg-gray-900 p-4 mx-20">
      <div className="container flex items-center justify-between">
        <h1 className="text-white text-xl font-bold"><Link to="/">Your Website Title</Link></h1>
        <nav className="flex space-x-4">
            <Link to="/mypage" className="text-gray-300 hover:text-white text-lg">My Page</Link>
            <Link to="/pricing" className="text-gray-300 hover:text-white text-lg">Pricing</Link>
            <Link to="/products" className="text-gray-300 hover:text-white text-lg">Products</Link>
            <Link to="/signup" className="text-gray-300 hover:text-white text-lg">Sign Up</Link>
            <Link to="/login" className="text-gray-300 hover:text-white text-lg">Login</Link>
        </nav>
      </div>
    </header>
  )
}

export default Header