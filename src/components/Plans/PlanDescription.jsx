/* eslint-disable react/prop-types */

const PlanDescription = () => {
    
  return (
      <div>
        <div className="text-center mt-16">
          <h5 className="text-lg font-bold">Detailed Feature Comprasion</h5>
        </div>
        <div className="mt-8">
          <div className="overflow-x-auto">
            <table className="w-4/5 mx-auto border border-gray-300">
              <tbody>
                <tr className="border-b p-2">
                  <th><span>Plans</span></th>
                  <td>Free</td>
                  <td>Basic</td>
                  <td>Pro</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Price</th>
                  <td>$0/mo</td>
                  <td>$1/mo</td>
                  <td>$2/mo</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Short Links</th>
                  <td>10/mo</td>
                  <td>200/mo</td>
                  <td>30000/mo</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Link Clicks</th>
                  <td>Unlimited</td>
                  <td>Unlimited</td>
                  <td>Unlimited</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Default expiration</th>
                  <td>0/days</td>
                  <td>30/days</td>
                  <td>180/days</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Custom alias</th>
                  <td>-</td>
                  <td>+</td>
                  <td>+</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Custom domain</th>
                  <td>-</td>
                  <td>+</td>
                  <td>+</td>
                </tr>
                <tr className="border-b p-2">
                  <th>History</th>
                  <td>1/mo</td>
                  <td>12/mo</td>
                  <td>24/mo</td>
                </tr>
                <tr className="border-b p-2">
                  <th>Dashboard</th>
                  <td>-</td>
                  <td>+</td>
                  <td>+</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  )
}

export default PlanDescription