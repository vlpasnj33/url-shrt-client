/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */

const PricingPlan = ({ price, title, features, buttonText }) => {
  return (
      <div className="flex min-h-[300px] w-[220px] flex-col rounded-3xl mt-8 p-8 bg-gray-200">
        <h2 className="mb-5 text-xl font-medium mx-auto">{price}/month</h2>
        <h3 className="mb-5 flex items-end text-2xl font-black mx-auto">{title}</h3>
        <ul className="mb-10 flex flex-col gap-y-2 mx-auto">
          {features.map((feature) => ( 
            <li className="flex items-center">
              {feature}
            </li>
          ))}
        </ul>
        <button className="w-full bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600">
          {buttonText}
        </button>
      </div>
  )
}

export default PricingPlan