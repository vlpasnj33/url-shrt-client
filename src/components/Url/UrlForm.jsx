
const UrlForm = () => {
  return (
        <div className="mb-8">
            <h4 className="text-xl font-bold mb-2">URL Shortener</h4>
            <form className="flex flex-col space-y-4">
                <label className="flex flex-col">
                    Paste a long URL:
                    <input type="url" required className="mt-1 p-2 border rounded" />
                </label>
                <label className="flex flex-col">
                    Domain:
                    <input type="domain" className="mt-1 p-2 border rounded" />
                </label>
                <label className="flex flex-col">
                    Alias:
                    <input type="alias" className="mt-1 p-2 border rounded" />
                </label>
                <button type="submit" className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600">
                    Shorten URL
                </button>
            </form>
        </div>
  )
}

export default UrlForm