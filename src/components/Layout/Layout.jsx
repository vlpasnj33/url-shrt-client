/* eslint-disable react/prop-types */
import Background from "./Background"

const Layout = ({ children }) => {
  return (
    <div className="relative">
        <Background />
        <div className="relative z-10">
        {children}
        </div>
    </div>
  )
}

export default Layout