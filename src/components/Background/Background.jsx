
const Background = () => {
  return (
    <div className="absolute top-0 left-0 w-full h-full bg-gradient-to-r from-red-500 to-blue-500 z-0"></div>
  )
}

export default Background