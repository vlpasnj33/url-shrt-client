import { useEffect, useRef, useState } from 'react';
import { faCheck, faTimes, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';
import googleIcon from '../assets/Google.svg';
import facebookIcon from '../assets/Facebook.svg';
import linkedinIcon from '../assets/LinkedIn.svg';
import axios from '../../api/axios';

const USER_REGEX = /^[A-z][A-z0-9-_]{3,23}$/;
const EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;

const SignUp = () => {
  const userRef = useRef()
  const errRef = useRef();

  const [user, setUser] = useState('');
  const [validName, setValidName] = useState(false);
  const [userFocus, setUserFocus] = useState(false);

  const [email, setEmail] = useState('');
  const [validEmail, setValidEmail] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);

  const [pwd, setPwd] = useState('');
  const [validPwd, setValidPwd] = useState(false);
  const [pwdFocus, setPwdFocus] = useState(false);

  const [errMsg, setErrMsg] = useState('');
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    userRef.current.focus();
  }, [])

  useEffect(() => {
    const result = USER_REGEX.test(user);
    setValidName(result);
  }, [user])

  useEffect(() => {
    setValidEmail(EMAIL_REGEX.test(email));
  }, [email])

  useEffect(() => {
    setValidPwd(PWD_REGEX.test(pwd));
  }, [pwd])

  useEffect(() => {
    setErrMsg('');
  }, [user, email, pwd])

  const handleSubmit = async(e) => {
    e.preventDefault();
    const v1 = USER_REGEX.test(user);
    const v2 = EMAIL_REGEX.test(email);
    const v3 = PWD_REGEX.test(pwd);
    if (!v1 || !v2 || !v3) {
      setErrMsg("Invalid Entry");
      return;
    }
    
    try {
      await axios.post(import.meta.env.VITE_REGISTER_URL,
          JSON.stringify({user, email, pwd}),
          {
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
          }
      );
        setSuccess(true);
        setUser('');
        setEmail('');
        setPwd('');

    } catch (err) {
      if (!err?.response) {
        setErrMsg('No Server Response');
      } else if (err.response?.status === 409) {
          setErrMsg('Username Taken');
      } else {
          setErrMsg('Registration Failed')
      }
      errRef.current.focus();
    }
  }

  return (
    <>
      {success ? (
       <section>
        <h1>Success!</h1>
         <p>
          <Link to="/mypage" />
         </p>
       </section> 
       ) : (
          <section>
          <p ref={errRef} className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
          <form className="bg-gray-200 rounded-lg p-2 max-w-md mx-auto mt-8" onSubmit={handleSubmit}>
              <h2 className="text-xl font-semibold text-white-700 mb-4 text-center mx-auto">Create your account</h2>

              <label htmlFor="username" className="block mb-1">
                Name
                <FontAwesomeIcon icon={faCheck} className={validName ? "valid" : "hide"} />
                <FontAwesomeIcon icon={faTimes} className={validName || !user ? "hide" : "invalid"} />
              </label>
              <input 
                  type="text" 
                  id="username" 
                  name="username" 
                  ref={userRef}
                  className="w-full h-10 px-3 border rounded" 
                  placeholder="Name" 
                  onChange={(e) => setUser(e.target.value)}
                  value={user}
                  required 
                  autoComplete="off"
                  aria-invalid={validName ? "false" : "true"}
                  aria-describedby="uidnote"
                  onFocus={() => setUserFocus(true)}
                  onBlur={() => setUserFocus(false)}
              />

              <p id="uidnote" className={userFocus && user && !validName ? "instructions" : "offscreen"}>
                                <FontAwesomeIcon icon={faInfoCircle} />
                                4 to 24 characters.<br />
                                Must begin with a letter.<br />
                                Letters, numbers, underscores, hyphens allowed.
              </p>

              <label htmlFor="email" className="block mb-1 mt-4">
                Email
                <FontAwesomeIcon icon={faCheck} className={validEmail ? "valid" : "hide"} />
                <FontAwesomeIcon icon={faTimes} className={validEmail || !email ? "hide" : "invalid"} />
              </label>
              <input 
                  type="text" 
                  id="email" 
                  name="email" 
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  className="w-full h-10 px-3 border rounded" 
                  placeholder="Email" 
                  required 
                  aria-invalid={validEmail ? "false" : "true"}
                  aria-describedby="emailnote"
                  onFocus={() => setEmailFocus(true)}
                  onBlur={() => setEmailFocus(false)}
              />

              <p id="emailnote" className={emailFocus && !validEmail ? "instructions" : "offscreen"}>
                                <FontAwesomeIcon icon={faInfoCircle} />
                                Not valid email<br />
              </p>

              <label htmlFor="password" className="block mb-1 mt-4">
                Password
                <FontAwesomeIcon icon={faCheck} className={validPwd ? "valid" : "hide"} />
                <FontAwesomeIcon icon={faTimes} className={validPwd || !pwd ? "hide" : "invalid"} />
              </label>
              <input 
                  type="password" 
                  id="password" 
                  name="password"
                  onChange={(e) => setPwd(e.target.value)}
                  value={pwd} 
                  className="w-full h-10 px-3 border rounded" 
                  placeholder="Password" 
                  required 
                  aria-invalid={validPwd ? "false" : "true"}
                  aria-describedby="pwdnote"
                  onFocus={() => setPwdFocus(true)}
                  onBlur={() => setPwdFocus(false)}
              />

              <p id="pwdnote" className={pwdFocus && !validPwd ? "instructions" : "offscreen"}>
                                <FontAwesomeIcon icon={faInfoCircle} />
                                8 to 24 characters.<br />
                                Must include uppercase and lowercase letters, a number and a special character.<br />
                                Allowed special characters: <span aria-label="exclamation mark">!</span> <span aria-label="at symbol">@</span> <span aria-label="hashtag">#</span> <span aria-label="dollar sign">$</span> <span aria-label="percent">%</span>
              </p>

              <button 
                  type="submit" 
                  className="w-full bg-blue-500 text-white py-2 px-4 rounded mt-4 hover:bg-blue-600"
                  disabled={!validName || !validEmail || !validPwd ? true : false}    
              >
                    Create Account      
              </button>
              <div className="or-divider flex items-center mb-4 mt-4">
                <div className="or-line flex-1 h-px bg-gray-400"></div>
                <span className="or-text mx-2 text-gray-500">or via</span>
                <div className="or-line flex-1 h-px bg-gray-400"></div>
              </div>
              <div className="flex items-center justify-center space-x-2 mb-4">
                <a href="#" className="svg-icon"><img src={googleIcon} alt="Google Icon" className="w-8 h-8" /></a> 
                <a href="#" className="svg-icon"><img src={facebookIcon} alt="Facebook Icon" className="w-8 h-8" /></a>
                <a href="#" className="svg-icon"><img src={linkedinIcon} alt="LinkedIn Icon" className="w-8 h-8" /></a>
              </div>
              <p className="text-sm text-gray-500 mt-2 text-center">
                Already have an account? 
                <Link to="/login" className="text-blue-500 cursor-pointer"> Login</Link>
              </p>
          </form>
        </section>
       )}
    </>
  )
}

export default SignUp