import QrcodeStatistics from "./QrcodeStatistics"
import UrlStatistics from "../Url/UrlStatistics"

const UserHome = () => {
  return (
    <div className="max-w-2xl mx-auto mt-8 p-4 border border-gray-300 rounded">
      <h2 className="text-3xl font-bold mb-4">Welcome!</h2>
      <p>Usage this month: </p>
      <UrlStatistics />
      <QrcodeStatistics />
      <button className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600 mr-2">
        Shorten Url
      </button>
      <button className="bg-green-500 text-white py-2 px-4 rounded hover:bg-green-600">
        Create Qrcode
      </button>
    </div>
  )
}

export default UserHome