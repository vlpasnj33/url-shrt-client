import { useContext, useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom"
import googleIcon from '../assets/Google.svg';
import facebookIcon from '../assets/Facebook.svg';
import linkedinIcon from '../assets/LinkedIn.svg';
import AuthContext from "../../context/AuthProvider";
import axios from "../../api/axios";

const Login = () => {
  const { setAuth } = useContext(AuthContext);
  const userRef = useRef();
  const errRef = useRef();

  const [email, setEmail] = useState("");
  const [pwd, setPwd] = useState("");
  const [errMsg, setErrMsg] = useState("");
  const [succces, setSuccess] = useState(false);

  useEffect(() => {
    userRef.current.focus();
  }, [])

  useEffect(() => {
    setErrMsg('');
  }, [email, pwd])

  const handleSubmit = async(e) => {
    e.preventDefault();

    try {
      const response = await axios.post(import.meta.env.VITE_LOGIN_URL,
        JSON.stringify({email, pwd}),
        {
          headers: {'Content-Type': 'application/json'},
          withCredentials: true
        }
      );
      const token = response?.data?.token;
      setAuth({ email, pwd, token });
      setEmail("");
      setPwd("");
      setSuccess(true);
    } catch (err) {
      if (!err?.response) {
        setErrMsg('No Server Response');
      } else if (err.response?.status === 400) {
          setErrMsg('Missing Email or Password');
      } else if (err.response?.status === 401) {
          setErrMsg('Unauthorized');
      } else {
          setErrMsg('Login Failed');
      }
      errRef.current.focus();
    }
  }

  return (
      <>
        {succces ? (
          <section>
            <h1>Success!</h1>
            <p>
              <Link to="/mypage" />
            </p>
          </section>
        ) : (
          <section>
          <p ref={errRef} className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
          <form className="bg-gray-200 rounded-lg p-2 max-w-md mx-auto mt-8" onSubmit={handleSubmit}>
            <h2 className="text-xl font-semibold text-white-700 mb-4 text-center mx-auto">Log in and start sharing</h2>

            <label htmlFor="email" className="block mb-1">Email</label>
            <input 
                type="text" 
                id="email" 
                className="w-full h-10 px-3 border rounded" 
                placeholder="Email" 
                ref={userRef}
                autoComplete="off"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required 

            />

            <label htmlFor="password" className="block mb-1 mt-4">Password</label>
            <input 
                type="password" 
                id="password" 
                className="w-full h-10 px-3 border rounded" 
                placeholder="Password" 
                onChange={(e) => setPwd(e.target.value)}
                value={pwd}
                required 
            />

            <button type="submit" className="w-full bg-blue-500 text-white py-2 px-4 rounded mt-4 hover:bg-blue-600">Log in</button>
            <div className="or-divider flex items-center mb-4 mt-4">
              <div className="or-line flex-1 h-px bg-gray-400"></div>
              <span className="or-text mx-2 text-gray-500">or via</span>
              <div className="or-line flex-1 h-px bg-gray-400"></div>
            </div>
            <div className="flex items-center justify-center space-x-2 mb-4">
              <a href="#" className="svg-icon"><img src={googleIcon} alt="Google Icon" className="w-8 h-8" /></a> 
              <a href="#" className="svg-icon"><img src={facebookIcon} alt="Facebook Icon" className="w-8 h-8" /></a>
              <a href="#" className="svg-icon"><img src={linkedinIcon} alt="LinkedIn Icon" className="w-8 h-8" /></a>
            </div>
            <p className="text-sm text-gray-500 mt-2 text-center">
                Don’t have an account? 
                <Link to="/signup" className="text-blue-500 cursor-pointer"> Sign Up</Link>
            </p>
          </form>
        </section>
        )}
      </>
    )
}

export default Login