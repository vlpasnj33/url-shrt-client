import UserHome from "../components/UserHome"
import Header from "../components/Header"
import Footer from "../components/Footer"

const MyPage = () => {
  return (
    <div>
      <Header />
      <UserHome />
      <Footer />
    </div>
  )
}

export default MyPage