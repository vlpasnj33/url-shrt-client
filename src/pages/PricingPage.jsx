import Footer from "../components/Footer"
import Header from "../components/Header"
import PlanDescription from "../components/PlanDescription"
import PricingPlan from "../components/PricingPlan"

const PricingPage = () => {
  return (
    <div>
      <Header />
      <div className="flex justify-center gap-8">
        <PricingPlan 
          price="$0"
          title="Free Plan"
          features={['2 QR Codes/month', '10 links/month']}
          buttonText="Sign Up for Free"
        />
        <PricingPlan 
            price="$2"
            title="Basic Plan"
            features={['2 QR Codes/month', '10 links/month']}
            buttonText="Upgrade to Basic"
        />
        <PricingPlan
            price="$3" 
            title="Pro Plan"
            features={['2 QR Codes/month', '10 links/month']}
            buttonText="Upgrade to Pro"
        />
      </div>
      <PlanDescription />
      <Footer />
    </div>
  )
}

export default PricingPage