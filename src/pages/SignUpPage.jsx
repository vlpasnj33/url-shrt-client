import SignUp from "../components/SignUp"
import Footer from "../components/Footer"
import Header from "../components/Header"

const SignUpPage = () => {
  return (
    <div>
        <Header />
        <SignUp />
        <Footer />
    </div>
  )
}

export default SignUpPage