import About from "../components/About"
import Footer from "../components/Footer"
import Header from "../components/Header"

const ProductsPage = () => {
  return (
    <div>
      <Header />
      <About />
      <Footer />
    </div>
  )
}

export default ProductsPage