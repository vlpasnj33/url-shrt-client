import Footer from "../components/Footer"
import Header from "../components/Header"
import Main from "../components/Main"
import Layout from "../components/Layout"

const HomePage = () => {
  return (
    <Layout>
      <Header />
      <Main />
      <Footer />
    </Layout>
  )
}

export default HomePage